class FrogFruit extends GameObject
{
    speed = 10;

    constructor (x, y, sprite, layer)
    {
        super(x, y, sprite, layer);

        this.transform.scale = new vector(0.7, 0.7);
        this.renderer.subImage = Math.floor(Math.randomRange(0, 15));

        this.collider.enabled = true;
    }

    onCollision (obj)
    {
        if (obj instanceof FrogFloorTile)
        {
            obj.destroy();
            this.scene.destroyFruit(this);
        }
    }

    onDestroy ()
    {
        this.scene.addObject(new FrogDestroyer(this.transform.position.x, this.transform.position.y, this.scene.game.images.tile_floor_explosion, 0, .05, true));
    }

    update (deltaTime)
    {
        super.update(deltaTime);

        this.transform.position.y += this.speed * this.scene.gameSpeed * this.scene.gameSpeed * deltaTime;

        if (this.transform.position.y > this.scene.real_size.y)
        {
            this.scene.destroyFruit(this);
        }
    }
}