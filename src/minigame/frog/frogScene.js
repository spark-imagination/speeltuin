class FrogScene extends Scene
{
    //something
    fruitSpawnRateMin = 1;
    fruitSpawnRateMax = 2;
    fruitSpawnTimer = 0;

    fruitSpawnChance = .2;

    fruits = [];
    fruitPool = [];
    eatenFruit = [];

    gameSpeed = 1;

    start = false;
    paused = false;

    closeButton = null;

    closeBtnX = 0;
    closeBtnY = -140;

    posScaleY = 0;
    text_1_target_y;
    text_2_target_y;

    image_1_target_y;
    image_1_rotation = -1;

    spaceBarTween = null;
    screenTween = null;

    destructing = false;

    externalFruitCurrentPos = null;

    windowDestroyTweens = [];

    constructor (x, y, width, height, displayMode = 0)
    {
        super(x, y, width, height, displayMode);

        this.fruitSpawnTimer = this.fruitSpawnRateMax;

        this.text_1_target_y = this.real_size.y * .3;
        this.text_2_target_y = this.real_size.y * .62;
        this.image_1_target_y = this.real_size.y * .45;
        this.closeBtnX = this.real_size.x - 50;
    }

    onLoad ()
    {
        let pos = new vector(this.closeBtnX, this.closeBtnY);
        this.closeButton = this.addObject(new SubBtn(pos.x, pos.y, this.game.images.closeButton, 1));

        this.closeButton.isClicked.add(function ()
        {
            this.paused = false;
            this.posScaleY = 2.5;
            this.gameOver();
        }, this);

        this.externalFruitCurrentPos = new vector(this.real_position.x - this.game.images.fruit.width / 2, this.real_position.y - this.game.images.fruit.height / 2);

        this.maxPerXAxis = Math.ceil(this.real_size.x / this.game.images.fruit.width) + 2;
        this.maxPerYAxis = Math.ceil(this.real_size.y / this.game.images.fruit.height) + 2;

        this.doMenuScreenTween();

        this.doSpaceBarTweenIn();
    }

    update (deltaTime)
    {
        if (!this.paused)
        {
            super.update(deltaTime);
        }
        else
        {
            this.closeButton.update(deltaTime);
        }

        if (this.start && !this.destructing)
        {
            this.checkEscape();
            if (!this.paused)
                this.spawnFruits(deltaTime);

        }
        else if (!this.start && !this.destructing)
        {
            this.checkSpacebar();
        }

        if (this.destructing)
        {
            if (this.activeObjects.length > 0)
            {
                return;
            }
            if (this.windowDestroyTweens.length > 0)
            {
                let frogButton = MainMenu.Instance.menu.children.btnGames.menu.children.btnFrog;
                frogButton.moveToActiveState(800, () => frogButton.moveToInactiveState(800));

                this.windowDestroyTweens.forEach(t => t.start());
                this.windowDestroyTweens[0].onComplete(() => this.destroy());

                this.windowDestroyTweens = [];
            }
        }
    }

    gameOver ()
    {
        if (this.spaceBarTween)
            TWEEN.remove(this.spaceBarTween);
        if (this.screenTween)
            TWEEN.remove(this.screenTween);
        this.activeObjects.forEach(ao => ao.destroy());

        this.passEatenFruitsToSimpleImagesRenderer();

        this.destructing = true; // finish animations first
    }

    passEatenFruitsToSimpleImagesRenderer ()
    {

        let targetButton = MainMenu.Instance.menu.children.btnFruit;
        let targetVector = targetButton.targetPosition;

        this.eatenFruit.forEach(e => 
        {
            e.targetPosition = targetVector;
            e.targetSize = new vector(.5, .5);
            e.time = Math.floor(Math.randomRange(500, 700));
        });

        if (this.eatenFruit.length > 0)
        {
            SimpleImagesRenderTween.Instance.add(this.eatenFruit, () => targetButton.moveToActiveState(800), () => targetButton.moveToInactiveState(800));
        }

        this.eatenFruit = [];
    }

    increaseDifficulty ()
    {
        this.gameSpeed += .1;

        if (this.fruitSpawnRateMax > .1)
        {
            this.fruitSpawnRateMax -= .2;
        }
        else if (this.fruitSpawnRateMax == .2)
        {
            this.fruitSpawnRateMax = .1;
        }
        if (this.fruitSpawnRateMin > 0)
        {
            this.fruitSpawnRateMin -= .2;
        }
    }

    startGame ()
    {
        if (this.spaceBarTween)
        {
            this.spaceBarTween.stop();
        }

        this.doAwayMenuScreenTween();

        let frog = new PlayerFrog(60, this.real_size.y - 50, this.game.images.frog_basic, 0);

        let tileWidth = this.game.images.tile_floor_brown.width - 2;
        let tileWidthHalf = tileWidth / 2;

        for (let tx = tileWidthHalf - 4; tx < this.real_size.x; tx += tileWidth) 
        {
            this.addObject(new FrogFloorTile(tx, this.real_size.y - 30, this.game.images.tile_floor_brown, 0));
        }
        this.addObject(frog);

        this.start = true;
    }

    pauseGame ()
    {
        this.paused = true;

        this.doMenuScreenTween();
    }

    unPauseGame ()
    {
        this.doAwayMenuScreenTween();
    }

    spawnFruits (deltaTime)
    {
        this.fruitSpawnTimer -= deltaTime;
        if (this.fruitSpawnTimer < this.fruitSpawnRateMin) 
        {
            if (this.shouldSpawn() || this.fruitSpawnTimer < 0)
            {
                let fruit = this.getFromFruitPoolOrCreate();
                fruit.collider.enabled = true;
                fruit.speed = Math.randomRange(10, 20);
                fruit.transform.position = this.getRandomVectorPosition();
                this.setRandomSubImage(fruit);
                this.fruits.push(fruit);

                this.fruitSpawnTimer = this.fruitSpawnRateMax;
            }
        }
    }

    consumeFruit (fruit)
    {
        let currFruit = { position: this.getNextExternalFruitPosition(), size: new vector(.5, .5), image: game.images.fruit, subImage: fruit.renderer.subImage };
        this.eatenFruit.push(currFruit);

        new TWEEN.Tween(currFruit.size)
            .to({ x: 1, y: 1 }, 400)
            .easing(TWEEN.Easing.Elastic.Out)
            .start();

        this.destroyFruit(fruit);

        this.increaseDifficulty();

        FruitInventory.Instance.addToInventory(currFruit.subImage);
    }

    getNextExternalFruitPosition ()
    {
        if (this.eatenFruit.length == 0)
        {
            return this.externalFruitCurrentPos;
        }

        if (this.eatenFruit.length < this.maxPerXAxis)
        {
            return this.externalFruitCurrentPos = this.externalFruitCurrentPos.add(new vector(this.game.images.fruit.width, 0));
        }
        else if (this.eatenFruit.length - this.maxPerXAxis < this.maxPerYAxis)
        {
            return this.externalFruitCurrentPos = this.externalFruitCurrentPos.add(new vector(0, this.game.images.fruit.height));
        }
        else if (this.eatenFruit.length - this.maxPerYAxis < this.maxPerXAxis * 2)
        {
            return this.externalFruitCurrentPos = this.externalFruitCurrentPos.subtract(new vector(this.game.images.fruit.width, 0));
        }
        else if (this.eatenFruit.length - this.maxPerYAxis < this.maxPerYAxis * 2)
        {
            return this.externalFruitCurrentPos = this.externalFruitCurrentPos.subtract(new vector(0, this.game.images.fruit.height));
        }
    }

    destroyFruit (fruit)
    {
        if (this.fruits.contains(fruit))
        {
            this.fruits.remove(fruit);
            this.fruitPool.push(fruit);
            fruit.transform.position.x = 50000;
        }
    }

    getFromFruitPoolOrCreate ()
    {
        return this.fruitPool.length > 0 ? this.fruitPool.pop() : this.addObject(new FrogFruit(0, 0, this.game.images.fruit, 0));
    }

    setRandomSubImage (fruit)
    {
        fruit.renderer.subImage = Math.floor(Math.randomRange(0, fruit.renderer.sprite.subImages));
    }

    draw ()
    {
        super.draw();

        this.drawExternalFruit();
        this.drawMenu();
    }

    getRandomVectorPosition ()
    {
        return new vector
            (
                Math.floor(Math.randomRange(0, this.real_size.x)),
                0
            );
    }

    shouldSpawn ()
    {
        return Math.random() < this.fruitSpawnChance;
    }

    checkSpacebar ()
    {
        if (GameInput.isHeld(GameInput.keys[" "]))
        {
            this.startGame();
        }
    }

    checkEscape ()
    {
        if (GameInput.isPressed(GameInput.keys.Escape))
        {

            this.paused ? this.unPauseGame() : this.pauseGame();
        }
    }

    drawExternalFruit ()
    {
        this.eatenFruit.forEach(e =>
            this.game.images.fruit.draw(this.parentScene,
                e.position.x, e.position.y,
                this.game.images.fruit.width * e.size.x, this.game.images.fruit.height * e.size.y,
                0, 0, 0, 1, e.subImage));

    }

    drawMenu ()
    {
        if (!this.paused)
        {
            if (this.posScaleY < 2)
            {
                this.context.font = "20px Monospace";
                this.context.fillStyle = "black"; // sets the color to fill in the rectangle with

                let textData = this.context.measureText("Press");
                this.context.fillText("Press", this.real_size.x * .5 - (textData.width / 2), this.text_1_target_y * this.posScaleY, 72);

                textData = this.context.measureText("To Start");
                this.context.fillText("To Lick", this.real_size.x * .5 - (textData.width / 2) + 5, this.text_2_target_y * this.posScaleY, 72);

                let space = this.game.images.spacebar;
                space.draw(this, this.real_size.x * .5, this.image_1_target_y * this.posScaleY, space.width, space.height, this.image_1_rotation * 3, 0, 0, 1, 0);
            }
        }
        else
        {
            this.context.font = "20px Monospace";
            this.context.fillStyle = "black"; // sets the color to fill in the rectangle with

            let textData = this.context.measureText("Paused");
            this.context.fillText("Paused", this.real_size.x * .5 - (textData.width / 2), this.text_1_target_y * this.posScaleY * 2, 72);
        }
    }

    doMenuScreenTween ()
    {
        this.screenTween = new TWEEN.Tween(this)
            .to({ posScaleY: 1 }, 800)
            .easing(TWEEN.Easing.Elastic.Out)
            .start();
        this.doCloseButtonTweenIn();
    }

    doCloseButtonTweenIn ()
    {
        new TWEEN.Tween(this.closeButton.transform.position)
            .to({ y: 40 }, 800)
            .easing(TWEEN.Easing.Elastic.Out)
            .start();
    }

    doCloseButtonTweenOut ()
    {
        new TWEEN.Tween(this.closeButton.transform.position)
            .to({ y: -40 }, 800)
            .easing(TWEEN.Easing.Elastic.Out)
            .start();
    }

    doSpaceBarTweenIn ()
    {
        this.spaceBarTween = new TWEEN.Tween(this)
            .to({ image_1_rotation: 1 }, 500)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .yoyo(true)
            .repeat(Infinity)
            .start();
    }

    doAwayMenuScreenTween ()
    {
        this.doCloseButtonTweenOut();

        this.screenTween = new TWEEN.Tween(this)
            .to({ posScaleY: 2.5 }, 800)
            .easing(TWEEN.Easing.Elastic.InOut)
            .start().onComplete(() => this.paused = false);

    }
}