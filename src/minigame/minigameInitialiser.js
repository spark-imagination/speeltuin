class MinigameInitialiser 
{
    static StartFrogGame (parentScene, originVector, originSize)
    {
        let subScene = new FrogScene(144, 64, 416, 512, Scene.DisplayModes.absolute);

        let endPos = subScene.real_position.getCopy();
        let endSize = subScene.real_size.getCopy();

        subScene.position = originVector.getCopy();
        subScene.size = originSize.getCopy();

        this.SpawnWindowTween(subScene, endPos, endSize, 1000);

        subScene.gameSpeed = 1;

        subScene.windowDestroyTweens = this.DestroyWindowTween(subScene, originVector, originSize, 600);

        parentScene.loadChildScene(subScene);

        subScene.setPrimary();
    }


    static SpawnWindowTween (scene, endPosition, endSize, length)
    {
        new TWEEN.Tween(scene.position)
            .to({ x: endPosition.x, y: endPosition.y }, length)
            .easing(TWEEN.Easing.Elastic.Out)
            .start();

        new TWEEN.Tween(scene.size)
            .to({ x: endSize.x, y: endSize.y }, length)
            .easing(TWEEN.Easing.Elastic.Out)
            .start();
    }


    static DestroyWindowTween (scene, endPosition, endSize, length)
    {
        return [
            new TWEEN.Tween(scene.position)
                .to({ x: endPosition.x, y: endPosition.y }, length)
                .easing(TWEEN.Easing.Cubic.Out),

            new TWEEN.Tween(scene.size)
                .to({ x: endSize.x, y: endSize.y }, length)
                .easing(TWEEN.Easing.Cubic.Out)
        ];
    }
}