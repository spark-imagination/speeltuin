class SimpleImagesRenderTween extends GameObject
{
    static Instance;

    targetPosition = null;
    targetSize = null;

    imageDataPerRequest = [];
    //[
    //  { 
    //    data: {position, size, targetPosition, targetSize,image, subimage, time},
    //    onStart: val , 
    //    onDone:  val
    //  }
    //]

    tween = {
        default: (cont, req) => this.createTween(cont, req)
    };

    constructor (x, y, sprite, layer)
    {
        super(x, y, sprite, layer);

        SimpleImagesRenderTween.Instance = this;
    }

    add (arr, onStart, onDone, preset = "default")
    {
        this.imageDataPerRequest.push({ data: arr, onStart: onStart, onDone: onDone });
        let current = this.imageDataPerRequest[this.imageDataPerRequest.length - 1];
        arr.forEach(e => this.tween[preset](e, current));
    }

    update (deltaTime)
    {
        super.update(deltaTime);
        this.clean();
    }

    draw (context)
    {
        super.draw(context);
        this.drawImages();
    }

    clean ()
    {
        this.imageDataPerRequest = this.imageDataPerRequest.filter(e => !e.done);
    }

    drawImages ()
    {
        let context = this;
        this.imageDataPerRequest.forEach(arr => arr.data.forEach(e =>
            e.image.draw(
                context.scene,
                e.position.x, e.position.y,
                e.image.width * e.size.x, game.images.fruit.height * e.size.y,
                0, 0, 0, 1, e.subImage
            )
        ));
    }

    createTween (context, currentRequest)
    {
        if (context.targetPosition)
        {
            new TWEEN.Tween(context.position)
                .to({ x: context.targetPosition.x, y: context.targetPosition.y }, context.time)
                .easing(TWEEN.Easing.Quadratic.In)
                .start()
                .onStart(() => { currentRequest.onStart && currentRequest.onStart.call(this); })
                .onComplete(() => { context.done = true, this.checkArrayStatus(currentRequest), currentRequest.onDone && currentRequest.onDone.call(this); });
        }
        if (context.targetSize)
        {
            new TWEEN.Tween(context.size)
                .to({ x: context.targetSize.x, y: context.targetSize.y }, context.time)
                .easing(TWEEN.Easing.Quadratic.In)
                .start();
        }
    }

    checkArrayStatus (currentRequest)
    {
        let areAllDone = currentRequest.data.every(e => e.done);
        if (areAllDone)
        {
            currentRequest.done = true;
        }
    }
}