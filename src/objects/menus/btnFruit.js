class BtnFruit extends MoveableMenuItem
{
    fruitInventory = null;

    addedToScene ()
    {
        this.fruitInventory = this.scene.addObject(new FruitInventory(352, -500, this.scene.game.images.fruitInventory, 1));
    }

    onClick (btns)
    {
        MainMenu.Instance.menu.close();
        this.fruitInventory.moveToOpen();
    }
}