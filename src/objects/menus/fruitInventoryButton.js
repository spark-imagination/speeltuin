class FruitInventoryButton extends SubBtn
{
    type = -1;

    constructor (x, y, sprite, layer, type)
    {
        super(x, y, sprite, layer);
        this.type = type;
        this.renderer.subImage = type;
        this.transform.scale.x = 0.5;
        this.transform.scale.y = 0.5;
    }

    onClick (btns)
    {
        this.scene.addObject(FruitInventory.Instance.takeFromInventory(this.type));
        this.destroy();
        FruitInventory.Instance.moveToClose();
    }
}