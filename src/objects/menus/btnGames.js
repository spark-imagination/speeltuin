class BtnGames extends MoveableMenuItem
{
    buttons = [];
    buttonPadding = 10;
    state = 0;
    prevState = 1;

    constructor (sx, sy, ex, ey, sprite, layer, isMenu, tOpen = 600, tClose = 800)
    {
        super(sx, sy, ex, ey, sprite, layer, isMenu, tOpen, tClose);
    }

    addedToScene ()
    {
        this.initialiseChildren();
    }

    onClick (btn)
    {
        this.menu.onClick(btn);
    }

    initialiseChildren ()
    {
        let target = this.getNextTargetPosition();
        let buttonFrog = this.scene.addObject(new MoveableMenuItem(this.originPosition.x, this.originPosition.y, target.x, target.y, this.scene.game.images.btnFrog, 3));
        buttonFrog.onClick = function ()
        {
            MinigameInitialiser.StartFrogGame(this.scene, new vector(this.targetPosition.x - this.transform.size.x / 2, this.targetPosition.y - this.transform.size.y / 2), this.transform.size);
            MainMenu.Instance.menu.close();
        };

        buttonFrog.createActiveStateTween = function (time, onDone)
        {
            return MoveableMenuItem.prototype.createActiveStateTween(time, onDone, this)
                .to({ x: [this.targetPosition.x], y: [this.originPosition.y, this.originPosition.y, this.originPosition.y, this.targetPosition.y] }, time)
                .easing(TWEEN.Easing.Back.Out);
        };

        buttonFrog.createInactiveStateTween = function (time, onDone)
        {
            return MoveableMenuItem.prototype.createInactiveStateTween(time, onDone, this)
                .to({ x: [this.targetPosition.x, this.targetPosition.x, this.targetPosition.x, this.targetPosition.x, this.originPosition.x], y: [this.originPosition.y] }, time)
                .easing(TWEEN.Easing.Quintic.Out);
        };

        this.menu.addChildItem("btnFrog", buttonFrog);
    }

    getNextTargetPosition ()
    {
        let buttonArray = Object.values(this.menu.children);
        if (buttonArray == 0)
        {
            return new vector(this.transform.position.x + this.transform.size.x * 2 + this.buttonPadding, this.transform.position.y + this.buttonPadding);
        }
        else
        {
            let previous = buttonArray[buttonArray.length - 1];
            current.endPos = new vector(
                previous.targetPosition.x,
                previous.targetPosition.y + previous.transform.size.y + this.buttonPadding);
        }
    }
}