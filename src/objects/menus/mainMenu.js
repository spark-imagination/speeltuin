class MainMenu extends MoveableMenuItem
{
    static Instance;

    buttons = [];
    buttonPadding = 10;
    buttonXOffset = -32;

    openTargetX = 60;

    closedTargetX = -15;

    constructor (sx, sy, ex, ey, sprite, layer, isMenu, tOpen = 800, tClose = 1000)
    {
        super(sx, sy, ex, ey, sprite, layer, isMenu, tOpen, tClose);
        MainMenu.Instance = this;

    }

    addedToScene ()
    {

        this.initialiseChildren();

        this.menu.moveSelfOnOpen();
        this.menu.moveSelfOnClose();

        //this.menu.open(() => this.menu.close());
    }

    onClick (btn)
    {
        this.menu.onClick(btn);
    }

    initialiseChildren ()
    {
        let vectorOrigin = this.getNextOriginPosition();
        this.menu.addChildItem("btnFruit", this.scene.addObject(new BtnFruit(vectorOrigin.x, vectorOrigin.y, this.targetPosition.x + this.buttonXOffset, vectorOrigin.y, this.scene.game.images.btnFruit, 4)));

        vectorOrigin = this.getNextOriginPosition();
        this.menu.addChildItem("btnToys", this.scene.addObject(new BtnToys(vectorOrigin.x, vectorOrigin.y, this.targetPosition.x + this.buttonXOffset, vectorOrigin.y, this.scene.game.images.btnToys, 4)));

        vectorOrigin = this.getNextOriginPosition();
        this.menu.addChildItem("btnGames", this.scene.addObject(new BtnGames(vectorOrigin.x, vectorOrigin.y, this.targetPosition.x + this.buttonXOffset, vectorOrigin.y, this.scene.game.images.btnGames, 4, true)));

        vectorOrigin = this.getNextOriginPosition();
        this.menu.addChildItem("btnPlants", this.scene.addObject(new BtnPlants(vectorOrigin.x, vectorOrigin.y, this.targetPosition.x + this.buttonXOffset, vectorOrigin.y, this.scene.game.images.btnPlants, 4))
        );

    }

    getNextOriginPosition ()
    {
        let buttonArray = Object.values(this.menu.children);
        if (buttonArray.length == 0)
        {
            return new vector(this.transform.position.x + this.buttonXOffset, this.transform.position.y + this.transform.size.y + this.buttonPadding);
        }
        else
        {
            return new vector(this.transform.position.x + this.buttonXOffset, buttonArray[buttonArray.length - 1].transform.position.y + buttonArray[buttonArray.length - 1].transform.size.y + this.buttonPadding);

        }
    }

    //override for peekOnHover during closed state
    peekOnHover ()
    {
        if (!GameInput.mousePosition)
            return;
        if (this.collider.isInBounds(GameInput.mousePosition))
        {
            if (this.activeTween == null && (this.transform.position.x == this.originPosition.x && this.transform.position.y == this.originPosition.y))
            {
                this.createHoverTween(400, null, this).easing(TWEEN.Easing.Elastic.Out).start();
            }
        }
        else
        {
            if (this.activeTween == null &&
                (this.transform.position.x != this.originPosition.x || this.transform.position.y != this.originPosition.y) &&
                this.menu.state == MenuStates.closed)
            {
                this.createInactiveStateTween(400, null, this).start();
            }
        }
    }

    //override for small opening from closed state
    createHoverTween (time, onDone, context)
    {
        return super.createHoverTween(time, onDone, context)
            .to({ x: this.originPosition.x + 10 }, time);
    }
}