class MoveableMenuItem extends GameObject
{
    targetPosition = null;
    originPosition = null;

    parent = null;

    activeTween = null;

    shouldPeekOnHover = true;

    menu = null;

    constructor (sx, sy, ex, ey, sprite, layer, isMenu, tOpen = 800, tClose = 1000)
    {
        super(sx, sy, sprite, layer);
        this.originPosition = new vector(sx, sy);
        this.targetPosition = new vector(ex, ey);

        if (isMenu)
        {
            this.addMenu(tOpen, tClose);
        }
    }

    addMenu (tOpen, tClose)
    {
        this.menu = new BaseMenu(this, tOpen, tClose);
    }

    peekOnHover ()
    {
        if (!GameInput.mousePosition)
            return;
        if (this.collider.isInBounds(GameInput.mousePosition))
        {
            if (this.activeTween == null && (this.transform.position.x == this.targetPosition.x && this.transform.position.y == this.targetPosition.y))
            {
                this.createHoverTween(400, null, this).easing(TWEEN.Easing.Elastic.Out).start();
            }
        }
        else
        {
            if (this.activeTween == null &&
                (this.transform.position.x != this.targetPosition.x || this.transform.position.y != this.targetPosition.y) &&
                this.parent && this.parent.state == MenuStates.open)
            {
                this.createActiveStateTween(400, null, this).start();
            }
        }
    }

    update (deltaTime)
    {
        super.update(deltaTime);
        if (this.shouldPeekOnHover)
        {
            this.peekOnHover();
        }
    }

    moveOnParentOpen ()
    {
        this.parent.isOpened.add((a, b) => this.moveToActiveState(a, b), this);
    }

    moveOnParentClose ()
    {
        this.parent.isClosed.add((a, b) => this.moveToInactiveState(a, b), this);
    }

    moveToActiveState (time, onDone)
    {
        return this.createActiveStateTween(time, onDone, this).start();
    }

    moveToInactiveState (time, onDone)
    {
        return this.createInactiveStateTween(time, onDone, this).start();
    }

    createActiveStateTween (time, onDone, context)
    {
        let tween = new TWEEN.Tween(context.transform.position)
            .to({ x: context.targetPosition.x, y: context.targetPosition.y }, time)
            .easing(TWEEN.Easing.Bounce.Out)
            .onComplete(() => { context.activeTween = null; onDone && onDone.call(context); });
        return tween.onStart(() => context.stopActiveTween(tween));
    }

    createInactiveStateTween (time, onDone, context)
    {
        let tween = new TWEEN.Tween(context.transform.position)
            .to({ x: context.originPosition.x, y: context.originPosition.y }, time)
            .easing(TWEEN.Easing.Elastic.Out)
            .onComplete(() => { context.activeTween = null; onDone && onDone.call(context); });
        return tween.onStart(() => context.stopActiveTween(tween));
    }

    createHoverTween (time, onDone, context)
    {
        let tween = new TWEEN.Tween(context.transform.position)
            .to({ x: context.targetPosition.x + 10, y: context.targetPosition.y }, time)
            .easing(TWEEN.Easing.Elastic.In)
            .onComplete(() => { context.activeTween = null; onDone && onDone.call(context); });
        return tween.onStart(() => context.stopActiveTween(tween));
    }

    stopActiveTween (tween)
    {
        if (this.activeTween) 
        {
            this.activeTween.stop();
        }
        this.activeTween = tween;
    }
}