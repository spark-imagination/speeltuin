class FruitInventory extends GameObject
{
    closeButton = null;
    closeBtnX = 120;
    closeBtnY = -140;
    state = 0;

    hBounds = 120;
    vBounds = 120;
    vOffset = 20;

    static Instance = null;

    activeTween = null;

    fruitInventoryButtons = [];

    g_inv = null;
    get inventory ()
    {
        if (!this.g_inv)
        {
            let inv = CookieManager.getCookie("fruitInventory");
            if (inv.length <= 0)
                inv = "[]";

            this.g_inv = JSON.parse(inv);
            this.g_inv = this.g_inv || [];
        }

        return this.g_inv;
    }

    constructor (x, y, sprite, layer)
    {
        super(x, y, sprite, layer);
        FruitInventory.Instance = this;
    }

    saveInventory ()
    {
        CookieManager.setCookie("fruitInventory", JSON.stringify(this.inventory));
    }

    getFruitAmountOfType (type)
    {
        let count = 0;

        for (let i = 0; i < this.inventory.length; i++)
        {
            if (this.inventory[i] == type)
                count++;
        }

        return count;
    }

    takeFromInventory (type)
    {
        for (let i = 0; i < this.inventory.length; i++)
        {
            if (this.inventory[i] == type)
            {
                this.inventory.splice(i, 1);
                this.saveInventory();

                let newFruit = new ItemFruit(GameInput.mousePosition.x, GameInput.mousePosition.y, this.scene.game.images.fruit, 0);
                newFruit.setType(type);
                return newFruit;
            }
        }

        this.moveToClose();

        return null;
    }

    addToInventory (type, amount = 1)
    {
        for (let i = 0; i < amount; i++)
            this.inventory.push(type);

        this.saveInventory();
    }

    addedToScene ()
    {
        let pos = this.transform.position.add(new vector(this.closeBtnX, this.closeBtnY));
        this.closeButton = this.scene.addObject(new SubBtn(pos.x, pos.y, this.scene.game.images.closeButton));

        this.moveToClose();

        this.closeButton.isClicked.add(function ()
        {
            this.moveToClose();
        }, this);

        MainMenu.Instance.menu.isOpened.add(function ()
        {
            this.moveToClose();
        }, this);
    }

    update (deltaTime)
    {
        super.update(deltaTime);
        let pos = this.transform.position.add(new vector(this.closeBtnX, this.closeBtnY));
        this.closeButton.transform.position = pos;
    }

    updateFruitsPositions ()
    {
        let xp = -this.hBounds;
        let yp = -this.vBounds;

        for (let i = 0; i < this.fruitInventoryButtons.length; i++)
        {
            this.fruitInventoryButtons[i].transform.position.x = this.transform.position.x + xp;
            this.fruitInventoryButtons[i].transform.position.y = this.transform.position.y + yp + this.vOffset;

            yp += this.scene.game.images.fruit.height * this.fruitInventoryButtons[i].transform.scale.y;

            if (yp > this.vBounds)
            {
                yp = -this.vBounds;
                xp += this.scene.game.images.fruit.width * this.fruitInventoryButtons[i].transform.scale.x;
            }
        }
    }

    moveToOpen (onDone)
    {
        let context = this;
        let tween = new TWEEN.Tween(this.transform.position)
            .to({ y: 250 }, 1500)
            .easing(TWEEN.Easing.Elastic.Out)
            .onUpdate(() => { FruitInventory.Instance.updateFruitsPositions(); })
            .onComplete(() => { this.state = MenuStates.open; this.activeTween = null; onDone && onDone.call(context); });


        tween.onStart(() => { this.clearFruitButtons(); this.stopActiveTween(tween); this.createFruitButtons(); }).start();

        let spawnedAndBelowMax = true;

    }

    moveToClose (onDone)
    {
        let context = this;
        let tween = new TWEEN.Tween(this.transform.position)
            .to({ y: -500 }, 500)
            .easing(TWEEN.Easing.Back.In)
            .onUpdate(() => { FruitInventory.Instance.updateFruitsPositions(); })
            .onComplete(() => 
            {
                this.state = MenuStates.closed;
                this.activeTween = null;
                this.clearFruitButtons();

                onDone && onDone.call(context);
            });

        tween.onStart(() => this.stopActiveTween(tween)).start();
    }

    createFruitButtons ()
    {
        for (let t = 0; t < this.scene.game.images.fruit.subImages; t++)
        {
            let amount = Math.min(17, this.getFruitAmountOfType(t));

            for (let i = 0; i < amount; i++)
            {
                this.fruitInventoryButtons.push(this.scene.addObject(new FruitInventoryButton(-50, -50, this.scene.game.images.fruit, 2, t)));
            }
        }

    }

    clearFruitButtons ()
    {
        for (let i = 0; i < this.fruitInventoryButtons.length; i++)
            this.fruitInventoryButtons[i].destroy();

        this.fruitInventoryButtons = [];
    }

    stopActiveTween (tween)
    {
        if (this.activeTween) 
        {
            this.activeTween.stop();
        }
        this.activeTween = tween;
    }
}