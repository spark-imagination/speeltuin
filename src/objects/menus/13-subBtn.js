class SubBtn extends GameObject
{
    isClicked = null;

    constructor (x, y, sprite, layer)
    {
        super(x, y, sprite, layer);
        this.isClicked = new Signal();
    }

    onClick (btns)
    {
        this.isClicked.dispatch();
    }

    moveToActiveState (onDone)
    {
        this.createOpenTween(onDone);
    }

    moveToInactiveState (onDone)
    {
        this.createCloseTween(onDone);
    }

    createActiveStateTween (onDone)
    {
        let context = this;
        let tween = new TWEEN.Tween(this.transform.position)
            .to({ x: this.targetPosition.x, y: this.targetPosition.y }, this.tOpen)
            .easing(TWEEN.Easing.Bounce.Out)
            .onComplete(() => { this.activeTween = null; onDone && onDone.call(context); })
            .start();
        return tween.onStart(() => this.stopActiveTween(tween));
    }

    createInactiveStateTween (onDone)
    {
        let context = this;
        let tween = new TWEEN.Tween(this.transform.position)
            .to({ x: this.originPosition.x, y: this.originPosition.y }, this.tClose)
            .easing(TWEEN.Easing.Elastic.Out)
            .onComplete(() => { this.activeTween = null; onDone && onDone.call(context); })
            .start();
        return tween.onStart(() => this.stopActiveTween(tween));
    }

    stopActiveTween (tween)
    {
        if (this.activeTween) 
        {
            this.activeTween.stop();
        }
        this.activeTween = tween;
    }
}