class BaseMenu
{
    children = {};

    owner = null;

    state = 0;
    prevState = 1;

    isOpened = null;
    isClosed = null;

    tOpen = 0;
    tClose = 0;

    constructor (owner, tOpen = 800, tClose = 1000)
    {
        this.owner = owner;
        this.tOpen = tOpen;
        this.tClose = tClose;
        this.init();
    }

    openOnParentOpen ()
    {
        this.owner.parent.isOpened.add(() => this.open(), this);
    }

    closeOnParentClose ()
    {
        this.owner.parent.isClosed.add(() => this.close(), this);
    }

    init ()
    {
        this.isOpened = new Signal();
        this.isClosed = new Signal();
    }

    addChildItem (label, childItem)
    {
        this.children[label] = childItem;
        childItem.parent = this;
        childItem.moveOnParentOpen();
        childItem.moveOnParentClose();
        if (childItem.menu && childItem.menu instanceof BaseMenu)
        {
            childItem.menu.closeOnParentClose();
        }
    }

    moveSelfOnOpen ()
    {
        this.isOpened.add((a, b) => this.owner.moveToActiveState(a, b), this);
    }

    moveSelfOnClose ()
    {
        this.isClosed.add((a, b) => this.owner.moveToInactiveState(a, b), this);
    }

    onClick (btn)
    {
        this.prevState == MenuStates.open ?
            this.open() :
            this.close();
    }

    open (callback)
    {
        if (this.state == MenuStates.open)
        {
            return;
        }
        this.prevState = MenuStates.closed;
        this.state = MenuStates.moving;
        this.isOpened.dispatch(this.tOpen, () => this.notifyDone(MenuStates.open, callback));
    }

    close (callback)
    {
        if (this.state == MenuStates.closed || (this.state == MenuStates.moving && this.prevState == MenuStates.open))
        {
            return;
        }
        this.prevState = MenuStates.open;
        this.state = MenuStates.moving;
        this.isClosed.dispatch(this.tClose, () => this.notifyDone(MenuStates.closed, callback));
    }

    /**
     * gets called when children complete their movement. 
     * Callback is the one received from open(callback), and gets called only once per state change
     */
    notifyDone (state, callback)
    {
        if (this.state != state)
        {
            this.state = state;
            callback && callback.apply(this);
        }
    }


}
