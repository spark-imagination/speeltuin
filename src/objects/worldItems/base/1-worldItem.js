class WorldItem extends GameObject
{
    wi_wasHeld = false;
    wi_isHeld = false;
    grabbable = true;
    wi_grabbed = false;
    data = {};

    /**
     * IMPORTANT: Any extra parameters AFTER layer in extending classes MUST be optional!
     */
    constructor (x, y, sprite, layer)
    {
        super(x, y, sprite, layer);
        WorldObjectTracker.Track(this);
        this.collider.enabled = true;
    }

    onClick (btn)
    {
        if (this.grabbable)
            this.wi_grabbed = true;
    }

    onMouseHeld (btn)
    {
    }

    update (dt)
    {
        super.update(dt);

        let anyHeld = GameInput.mouseHeld();

        if (this.wi_grabbed && !(anyHeld.left || anyHeld.right || anyHeld.middle))
        {
            WorldObjectTracker.Save();
            this.wi_grabbed = false;
        }

        if (this.wi_grabbed)
            this.transform.position = GameInput.mousePosition;

        if (this.transform.position.x - this.transform.size.x / 2 < 0)
            this.transform.position.x++;

        if (this.transform.position.x + this.transform.size.x / 2 > 704)
            this.transform.position.x--;

        if (this.transform.position.y - this.transform.size.y / 2 < 0)
            this.transform.position.y++;

        if (this.transform.position.y + this.transform.size.x / 2 > 640)
            this.transform.position.y--;

    }

    onSaveRestored ()
    {

    }

    onCollision (o2)
    {
        if (o2 instanceof WorldItem)
        {
            this.transform.position = this.transform.position.add(this.transform.position.subtract(o2.transform.position).normalize().stretch(o2.collider.widestDiagonal * 0.05));
        }
    }
}