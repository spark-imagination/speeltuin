class ItemFruit extends WorldItem
{
    constructor (x, y, sprite, layer)
    {
        super(x, y, sprite, layer);
    }

    setType (fruitType)
    {
        this.renderer.subImage = fruitType;
        this.data.fruitType = fruitType;
    }

    onSaveRestored ()
    {
        this.renderer.subImage = this.data.fruitType;
    }
}