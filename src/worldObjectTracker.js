class WorldObjectTracker
{
    static trackedObjects = [];

    static Track (obj)
    {
        WorldObjectTracker.trackedObjects.push(obj);
    }

    static Save ()
    {
        let items = [];
        for (let i = 0; i < WorldObjectTracker.trackedObjects.length; i++)
        {
            if (WorldObjectTracker.trackedObjects[i] && !WorldObjectTracker.trackedObjects[i].destroyed)
            {
                items.push(
                    {
                        type: WorldObjectTracker.trackedObjects[i].constructor.name,
                        x: WorldObjectTracker.trackedObjects[i].transform.position.x,
                        y: WorldObjectTracker.trackedObjects[i].transform.position.y,
                        rotation: WorldObjectTracker.trackedObjects[i].transform.rotation,
                        spriteName: WorldObjectTracker.trackedObjects[i].renderer.sprite.name,
                        layer: WorldObjectTracker.trackedObjects[i].layer,
                        data: WorldObjectTracker.trackedObjects[i].data
                    });
            }
        }

        let toSave = JSON.stringify(items);

        CookieManager.setCookie("worldItems", toSave);

        console.log("saved world objects", toSave);
    }

    static Load (targetScene)
    {
        let json = CookieManager.getCookie("worldItems");
        if (json.length <= 0)
            json = "[]";

        let list = JSON.parse(json);


        console.log("Start load!", list);

        for (let i = 0; i < list.length; i++)
        {
            console.log("LOADING WORLD OBJECT", list[i].type);
            let obj = WorldObjectTracker.instantiate(list[i].type, list[i].x, list[i].y, game.images[list[i].spriteName], list[i].layer);
            obj.data = list[i].data;
            obj.onSaveRestored();
            targetScene.addObject(obj);
        }
    }

    static instantiate (className, x, y, sprite, layer)
    {
        var o, f, c;
        c = eval(className);
        o = new c(x, y, sprite, layer);
        return o;
    }
}